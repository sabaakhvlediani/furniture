package com.example.project8

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        logInButton.setOnClickListener {
            if (emailEditText.text.toString().isNotEmpty() || passwordEditText.text.toString()
                    .isNotEmpty()
            ) {
                Toast.makeText(this, "SignUp is Success!", Toast.LENGTH_LONG).show()

            } else {
                Toast.makeText(this, "Email format is not Correct", Toast.LENGTH_LONG).show()
            }
            logIn()
        }
    }

    private fun logIn() {
        val email: String = emailEditText.text.toString()
        val password: String = passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()
        ) {
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        d("Sign In", "signInWithEmail:success")
                        val user = auth.currentUser
                        openDashboard()
                    } else {
                        // If sign in fails, display a message to the user.
                        d("Sign in", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                    // ...
                }
        }
    }

    private fun openDashboard() {
        val intent = Intent(this, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
