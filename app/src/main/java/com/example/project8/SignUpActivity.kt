package com.example.project8

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_authentication.signUpButton
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        signUpButton.setOnClickListener {
            if (emailEditText.text.toString().isNotEmpty() || passwordEditText.text.toString()
                    .isNotEmpty()
            ) {
                Toast.makeText(this, "SignUp is Success!", Toast.LENGTH_LONG).show()

            } else {
                Toast.makeText(this, "Email format is not Correct", Toast.LENGTH_LONG).show()
            }
            signUp()
        }
    }


    private fun signUp() {
        val email: String = emailEditText.text.toString()
        val password: String = passwordEditText.text.toString()
        val repeatPassword: String = repeatEditText.text.toString()


        if (email.isNotEmpty() &&
            password.isNotEmpty() && repeatPassword.isNotEmpty()
        ) {
            if (password == repeatPassword) {
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("SignUp", "createUserWithEmail:success")
                            val user = auth.currentUser
                            openDashboard()
                        } else {
                            // If sign in fails, display a message to the user.
                            d("SignUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
    }


    private fun openDashboard() {
        val intent = Intent(this, AuthenticationActivity()::class.java)
        startActivity(intent)
    }
}
